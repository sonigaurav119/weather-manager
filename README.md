# Weather App
Application to view weather updates. It's easy to scale and reuse with ability to customize styles through scss variables.

## Framework
#### [Angular(7+)](https://angular.io/)

## Libraries
##### UI Library - [Angular Material](https://material.angular.io/) 
##### Task runner - [Angualr cli](https://cli.angular.io/)
##### Weather API - [Open WeatherMap](https://openweathermap.org/api)

##### Test runner Framework - [Jest](https://jestjs.io/docs/en/api)
###### Note:- We are using jest because it's faster than karma in the following ways:-
* It runs test in parllel
* It runs the previously failed test first
* It caches the files that need to be run
* It uses JSDOM instead of actual DOM which is faster
* It automatically find tests related to changed files

## Architecture
![alt text](./src/assets/weather-app-architecture.jpg "Architecture of app")
#### Modules:-

##### AppModule
 Main Module of the application. Act as a starting point of the app.
##### CoreModule
 Module that contains global logic such as **global error handler**, **services**(such as logger, notification), **components**(such as header, footer) and **modules**(such as AuthModule)
##### SharedModule
 A module that contains shared logic such as constants, reusable services, components and modules(Component Module).
##### ComponentModule
 A module that contains all the generic components that can be used throughout the applications. Examples:- Loader, notification, error, toolbar, cards etc.
##### MaterialModule
A module that exports all the angular material modules that are used inside the app. Created this module to keep our code clean and readble.
##### WeatherReportsModule
 A feature module to show the weather of five cites. It has one service:-

  **WeatherReportService** - A service that is local to this module. Purpose of this service to provide support to the components of WeatherReportsModule by providing various methods such as **transformToWeatherCard** whose job is to transform the data into format understand by **WeatherCardComponent**.

##### WeatherForecastsModule
 A feature module to show the forecast of next hours.
 
#### Services:-
##### WeatherService
 WeatherService is a generic service(singleton) to fetch info related to weather via [Open Weather api](https://openweathermap.org/api) and map it to the format that is understood by the app. The purpose of mapping is if tomorrow a requirement comes to change **open weather API** to some other API then we need to change in one place.
 
##### LoggerService
 LoggerService is a generic singleton service to log message to console. It will log message to console only in devMode by decorating the methods(log, error and warn). Inside prodMode it will not log anything to console.
 
##### NotificationService
 NotificationService is a generic singleton service to show notification messages on UI. It internally uses ***matSnackbar*** service which expects component and config of type ***MatSnackBarConfig***  and then renders toast message on UI.

## Globar Error Handler
We are using [ErrorHandler](https://angular.io/api/core/ErrorHandler) to handle error globally. Any error message such as http error or client side error comes here. Since it's a single place to catch all the errors we can use it to send some notifications to channels(eg. slack, hipchat) about the error occured in app or we can post this error to some backend api which will dumps it to some database so that we can have records of all errros.

## Scss
 Using scss here to maintain CSS in a better way, Also using **BEM** notation for better readability of scss code.
 
 **Mixins** - **Reusable** scss code that can be included into any class in order to handle the duplicacy of the code. Also, it makes our scss code more maintainable.
 
 **Variables** - using scss variables for managing colors, spacing, etc. so that if there is need to change it then we need to update a single place.
 
 **Theming** - We are creating our custom theme by using ***angular-material-theme*** mixin which expects color palette and create theme for us. We can create dark theme as well.
## Unit Testcases
We are using jest as test runner and also as an assertion library. We can get a coverage report. You need to run ```npm test``` command. It will run all your test cases and create HTML coverage report for you(via ***istanbul***). For more commands, please look into **package.json** file. Also we have our own ***test-helpers*** and ***test-builders*** which helps to mock services for components and create mocked data.
![alt text](./src/assets/test-report.jpg "Test report")

## Reusable Components
#### Spinner
***Spinner*** is a component that can be used to show the loading state. It can be easily customized by the inputs.

#### Notification
***Notification*** is a component that can be used to show a toast message on the UI. Message can be either success, error or warnings. It can be easily customized by the inputs.

#### Toolbar
***Toolbar*** is a component that is displayed at the top of the page. It accepts the theme color as input property. All the tags that come inside toolbar content are rendered inside it via ```ng-content```.

#### WeatherCard
***WeatherCard*** is a card component that displays weather info in a nicer way. It accepts data that implements ***WeatherCard*** interface. It has the ability to show ***loading state*** and ***error state*** based on the data. It also emits an event of type ***WeatherCard*** whenever the user clicks on a card.

#### TemperatureCard
***TemperatureCard*** is a card component that display temperature info in nicer way. It accepts data that implements ***TempratureCard*** interface.

#### Error
***Error*** is a component that displays an error to the user if API gives an error. It accepts the message as input and renders it on the screen.
 
# Features!
  - **LoggerService** to log message to console in devmode only.
  - **NotificationService** to notify the user about the error.
  - **Global error handler** to handle the error at a single place(may be used to send notifications on channels such as slack etc).
  - **Lazy loaded** modules to reduce the main bundle size.
  - Custom **Preloading strategy** to eagerly fetch bundle in order to reduce the load time of the lazy-loaded module.
  - Easily extendable **reusable components**(such as weather card, temperature card, loaders, etc).
  - Easy to update CSS via **scss variables** and **mixins**
  - Easy to manage and extend cities(five cities whose weather need to be shown) via **constants**.
  - **skeleton** based loaders.
  - **Responsive** design.
  - **Unit test** covered code to ensure quality.
  - **Generic weather service** that can be used in fetch weather info based on the query params.


## Some of the patterns that are used indirectly
>**Singleton** (By default services that are created in angular)

> **Observer** (Angular highly depends on rxjs library which is based on observables that use observer pattern)

## What can we do more?
However, this application has enough features to extend and reuse. But still, there are chances to make it better and more approachable.
> We can use **ngrx store** to implement **redux pattern** in our app that helps in **state management** of the application

> We can use **ngx-translate** module to provide **multilingual** support of the app.