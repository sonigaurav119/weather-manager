export const environment = {
  production: true,
  endPoint: 'https://openweathermap.org/data/2.5',
  appId: 'b6907d289e10d714a6e88b30761fae22',
  weatherIconPath: 'assets/'
};
