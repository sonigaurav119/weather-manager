import { of } from 'rxjs';
import { weatherReport, city } from '../../../builders/weather-report-builder';
import { weatherForcast } from '../../../builders/weather-forcast-builder';

export class MockWeatherService {

    constructor() { }

    getCurrentWeather(_: string | number[]) {
        return of({ ...weatherReport().getReport() });
    }

    getweatherForcast(_: string) {
        return of({
            city, count: 5, forcastData: [
                weatherForcast().getForcast(),
                { ...weatherForcast().getForcast(), main: { temp: 16.53, temp_max: 16.53, temp_min: 13.62, humidity: 66 } },
                {
                    ...weatherForcast().getForcast(),
                    main: { temp: 15.74, temp_max: 15.74, temp_min: 13.15, humidity: 69, pressure: 1010.3 }
                },
                { ...weatherForcast().getForcast(), main: { temp: 14.71, temp_max: 14.71, temp_min: 13.25, humidity: 79 } },
                { ...weatherForcast().getForcast(), main: { temp: 13.85, temp_max: 13.85, temp_min: 13.12, humidity: 83 } },
            ]
        });
    }
}
