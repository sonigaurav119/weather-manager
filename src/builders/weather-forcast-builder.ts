import { Temperature, Wind, Weather, WeatherForcast } from '../app/shared/services/weather.model';

export class WeatherForcasttBuilder implements WeatherForcast {
    dt: number;
    // tslint:disable-next-line: variable-name
    dt_txt: string;
    main: Temperature;
    wind: Wind;
    weather: Weather[];
    clouds: { all: number };

    constructor({ dt = 1558645200, dt_txt = '2019-05-23 21:00:00',
        main = {
            humidity: 66, grnd_level: 1015.9, pressure: 1028, temp: -10.5, temp_max: -10, temp_min: -11
        },
        wind = { deg: 200, speed: 5 }, id = 524901, clouds = { all: 0 },
        weather = [{ id: 800, main: 'Clear', description: 'clear sky', icon: '0n' }],
    } = {}) {
        this.main = main;
        this.weather = weather;
        this.clouds = clouds;
        this.wind = wind;
    }

    getForcast(): WeatherForcast {
        return {
            main: this.main,
            weather: this.weather,
            dt: this.dt,
            dt_txt: this.dt_txt,
            clouds: this.clouds,
            wind: this.wind
        };
    }
}

export const weatherForcast = (params?: WeatherForcast) => new WeatherForcasttBuilder({ ...params });
