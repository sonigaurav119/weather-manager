import { WeatherReport, Coord, Temperature, Wind, Weather, City } from '../app/shared/services/weather.model';
import { CITIES } from 'src/app/shared/constants/cities';

export class WeatherReportBuilder implements WeatherReport {
    coord: Coord;
    main: Temperature;
    wind: Wind;
    weather: Weather[];
    id: number;
    clouds: { all: number };
    name: string;
    visibility: number;

    constructor({ coord = { lat: 55.75, lon: 37.62 },
        main = {
            humidity: 66, grnd_level: 1122.9, pressure: 1028, temp: -10.5, temp_max: -10, temp_min: -11
        },
        wind = { deg: 200, speed: 5 }, id = 524901, clouds = { all: 0 },
        weather = [{ id: 800, main: 'Clear', description: 'clear sky', icon: '0n' }],
        visibility = 10000, name = CITIES[0].name } = {}) {
        this.coord = coord;
        this.main = main;
        this.weather = weather;
        this.id = id;
        this.clouds = clouds;
        this.name = name;
        this.visibility = visibility;
        this.wind = wind;
    }

    getReport(): WeatherReport {
        return {
            coord: this.coord,
            main: this.main,
            weather: this.weather,
            id: this.id,
            clouds: this.clouds,
            name: this.name,
            visibility: this.visibility,
            wind: this.wind
        };
    }
}

export const weatherReport = (params?: WeatherReport) => new WeatherReportBuilder({ ...params });
export const city: City = { coords: { lat: 55.75, lon: 37.62 }, country: 'NE', name: 'Amsterdam', id: 2759794 };
