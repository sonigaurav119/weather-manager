import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WeatherReportsComponent } from './weather-reports/weather-reports.component';
import { AppPreloader } from './app-preloading-strategy';

const routes: Routes = [
  {
    path: '',
    component: WeatherReportsComponent
  },
  {
    path: 'forcast',
    // Lazily load the feature modules to reduce the main bundle size
    loadChildren: './weather-forcast/weather-forcast.module#WeatherForcastModule',
    data: {
      preload: true
    }
  }
];

// Using preloading strategy to fetech next possible module in order to reduce delay time for lazily loaded module
@NgModule({
  imports: [RouterModule.forRoot(routes, { preloadingStrategy: AppPreloader })],
  providers: [AppPreloader],
  exports: [RouterModule]
})
export class AppRoutingModule { }
