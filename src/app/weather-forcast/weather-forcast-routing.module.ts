import { NgModule } from '@angular/core';
import { Route, RouterModule } from '@angular/router';

import { WeatherForcastComponent } from './weather-forcast.component';

export const routes: Route[] = [
    { path: ':id', component: WeatherForcastComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class WeatherForcastRoutingModule { }
