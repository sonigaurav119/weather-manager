import { NO_ERRORS_SCHEMA } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

import { WeatherForcastComponent } from './weather-forcast.component';
import { WeatherService } from '../shared/services/weather.service';
import { MockWeatherService } from '../../helpers/mocks/services/weather.service.mock';
import { city } from '../../builders/weather-report-builder';
import { throwError } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';

const moduleImports = [HttpClientTestingModule, RouterTestingModule];
const components = [WeatherForcastComponent];
const services = [{ provide: WeatherService, useClass: MockWeatherService }];


describe('WeatherForcastComponent', () => {
  let component: WeatherForcastComponent;
  let fixture: ComponentFixture<WeatherForcastComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: moduleImports,
      declarations: components,
      schemas: [
        NO_ERRORS_SCHEMA
      ],
      providers: services
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WeatherForcastComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have list of forcast details', () => {
    expect(component.forcastData.length).toEqual(5);
  });

  it('should have temperature details, weather details hourly based', () => {
    const expectedResult = [
      { label: 'Atmospheric Pressure', value: component.currentWeather.main.grnd_level, unit: 'hPa' },
      { label: 'Humidity', value: component.currentWeather.main.humidity, unit: '%' },
      { label: 'Pressure', value: component.currentWeather.main.pressure, unit: 'hPa' }
    ];
    component.forcastData.forEach((forcast) => {
      expect(forcast.main.temp).toBeDefined();
      expect(forcast.main.temp_min).toBeDefined();
      expect(forcast.main.temp_max).toBeDefined();
      expect(forcast.weather[0].description).toBeDefined();
      expect(forcast.weather[0].icon).toBeDefined();
    });
    component.currentWeatherParameters.forEach((weatherParameter, index: number) => {
      expect(weatherParameter.label).toEqual(expectedResult[index].label);
      expect(weatherParameter.value).toEqual(expectedResult[index].value);
      expect(weatherParameter.unit).toEqual(expectedResult[index].unit);
    });
  });

  it('should have temperature cards rendered on ui', () => {
    const temperatureCard = fixture.debugElement.query(By.css('bb-temprature-card'));
    expect(temperatureCard.nativeElement).toBeDefined();
  });

  it('should have city info rendered on ui', () => {
    const cityNameEl = fixture.debugElement.query(By.css('.forcast__city-name'));
    const cityCountryEl = fixture.debugElement.query(By.css('.forcast__city-country'));
    expect(cityNameEl.nativeElement.textContent).toEqual(city.name);
    expect(cityCountryEl.nativeElement.textContent).toEqual(city.country);
  });

  it('should have render the current temp on ui', () => {
    const currentTempEl = fixture.debugElement.query(By.css('.forcast__current-temp'));
    const currentWeatherDescEl = fixture.debugElement.query(By.css('.forcast__description'));
    expect(currentTempEl.nativeElement.textContent).toEqual(component.currentWeather.main.temp + '℃');
    expect(currentWeatherDescEl.nativeElement.textContent.trim().toLowerCase()).toEqual(component.currentWeather.weather[0].description);
  });

  it('should have render atmospheric pressure, humidity and pressure on ui', () => {
    const forcastMainLableEls = fixture.debugElement.queryAll(By.css('.forcast__label'));
    const forcastMainValueEls = fixture.debugElement.queryAll(By.css('.forcast__value'));
    // expect Atmospheric Pressure
    expect(forcastMainLableEls[0].nativeElement.textContent).toEqual('Atmospheric Pressure');
    expect(forcastMainValueEls[0].nativeElement.textContent).toEqual(component.currentWeather.main.grnd_level + 'hPa');

    // expect Humidity
    expect(forcastMainLableEls[1].nativeElement.textContent).toEqual('Humidity');
    expect(forcastMainValueEls[1].nativeElement.textContent).toEqual(component.currentWeather.main.humidity + '%');

    // expect Pressure
    expect(forcastMainLableEls[2].nativeElement.textContent).toEqual('Pressure');
    expect(forcastMainValueEls[2].nativeElement.textContent).toEqual(component.currentWeather.main.pressure + 'hPa');
  });

});

describe('WeatherForcastComponent--error', () => {
  let component: WeatherForcastComponent;
  let fixture: ComponentFixture<WeatherForcastComponent>;
  const error = new HttpErrorResponse({status: 400, statusText: 'Bad request'});
  let weatherService: WeatherService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: moduleImports,
      declarations: components,
      schemas: [
        NO_ERRORS_SCHEMA
      ],
      providers: services
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WeatherForcastComponent);
    weatherService = TestBed.get(WeatherService);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should not have list of forcast details', () => {
    // Reset component forcast data
    component.forcastData = undefined;
    spyOn(weatherService, 'getweatherForcast').and.returnValue(throwError(error));
    component.ngOnInit();
    expect(component.forcastData).toBeUndefined();
    expect(component.showLoader).toEqual(false);
  });

});

