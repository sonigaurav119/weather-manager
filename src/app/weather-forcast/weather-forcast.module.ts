import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WeatherForcastComponent } from './weather-forcast.component';
import { WeatherForcastRoutingModule } from './weather-forcast-routing.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [WeatherForcastComponent],
  imports: [
    CommonModule,
    FlexLayoutModule,
    SharedModule,
    WeatherForcastRoutingModule
  ]
})
export class WeatherForcastModule { }
