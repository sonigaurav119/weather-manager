import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { switchMap, catchError } from 'rxjs/operators';

import { WeatherService } from '../shared/services/weather.service';
import { WeatherForcast, City } from '../shared/services/weather.model';
import { WeatherIcons } from '../shared/constants/icons-mappper';
import { throwError } from 'rxjs';
import { LoggerService } from '../core/services/logger/logger.service';


@Component({
  selector: 'bb-weather-forcast',
  templateUrl: './weather-forcast.component.html',
  styleUrls: ['./weather-forcast.component.scss']
})
export class WeatherForcastComponent implements OnInit {

  city: City;
  forcastData: WeatherForcast[];
  currentWeather: WeatherForcast;
  currentWeatherParameters: { label: string, value: number, unit: string }[] = [];
  showLoader = false;
  iconMapper = { ...WeatherIcons };
  constructor(
    private route: ActivatedRoute,
    private weatherService: WeatherService,
    private logger: LoggerService) { }

  ngOnInit() {
    this.showLoader = true;
    // Using switchMap to unsubscribe the previous subscription and switch to other subscription.
    // Here unsubscribe paramMap subscription and step up to weather service
    this.route.paramMap.pipe(
      switchMap(params => this.weatherService.getweatherForcast(params.get('id'))),
      catchError(error => {
        // hide loader in case of error
        this.showLoader = false;
        this.logger.log('Error occured while fetching forecast data');
        return throwError(error);
      })
    ).subscribe(res => {
      this.forcastData = res.forcastData;
      this.currentWeather = { ...this.forcastData[0] };
      this.setCurrentWeatherParameters(this.currentWeather);
      this.city = res.city;
      this.showLoader = false;
      this.logger.log('WeatherForcast data fetched successfully');
    });
  }

  /**
   * Add forcast info parameters to currentWeatherParameters array.
   * Setting them into array so that we can remove duplicacy on html side via *ngFor.
   * @param currentWeather of type WeatherForcast
   * @returns void
   */
  setCurrentWeatherParameters(currentWeather: WeatherForcast): void {
    this.currentWeatherParameters = [
      { label: 'Atmospheric Pressure', value: currentWeather.main.grnd_level, unit: 'hPa' },
      { label: 'Humidity', value: currentWeather.main.humidity, unit: '%' },
      { label: 'Pressure', value: currentWeather.main.pressure, unit: 'hPa' }
    ];
  }

}
