import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';

import { SharedModule } from './shared/shared.module';
import { CoreModule } from './core/core.module';
import { WeatherReportsModule } from './weather-reports/weather-reports.module';
import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    CoreModule,
    SharedModule,
    AppRoutingModule,
    WeatherReportsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
