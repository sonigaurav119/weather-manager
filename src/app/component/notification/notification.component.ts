import { Component, Inject, Input } from '@angular/core';
import { MAT_SNACK_BAR_DATA } from '@angular/material/snack-bar';

@Component({
  selector: 'bb-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss']
})
export class NotificationComponent {

  /** Type of the notification such as error, success or warning */
  @Input() type: string;
  /** Title represents the heading of notification */
  @Input() title: string;
  /** Message that gives the information to the user  */
  @Input() message: string;

  /** Either pass data as inputs or pass it as injectionToken of type MAT_SNACK_BAR_DATA which has all required properties */
  constructor(@Inject(MAT_SNACK_BAR_DATA) public data: { title: string, message: string, type: string }) {
    this.type = data.type || 'default';
    this.title = data.title || '';
    this.message = data.message || '';
  }

}
