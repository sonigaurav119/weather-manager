import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotificationComponent } from './notification.component';
import { MatSnackBar, MAT_SNACK_BAR_DATA } from '@angular/material/snack-bar';
import { MatSnackBarMock } from '../../../helpers/mocks/services/mat-snackbar.service.mock';
import { By } from '@angular/platform-browser';

describe('NotificationComponent--with injectionToken<MAT_SNACK_BAR_DATA>', () => {
  let component: NotificationComponent;
  let fixture: ComponentFixture<NotificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NotificationComponent],
      schemas: [
        NO_ERRORS_SCHEMA
      ],
      providers: [
        { provide: MatSnackBar, useClass: MatSnackBarMock },
        { provide: MAT_SNACK_BAR_DATA, useValue: { type: 'error', title: 'Error', message: 'Error occured' } }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have title, type and message properties on component', () => {
    expect(component.title).toEqual('Error');
    expect(component.type).toEqual('error');
    expect(component.message).toEqual('Error occured');
  });

  it('should have title, type and message properties render on ui', () => {
    const errorTypeEl = fixture.debugElement.query(By.css('.notification__error'));
    const errorTitleEl = fixture.debugElement.query(By.css('.notification__title'));
    const errorMessagEl = fixture.debugElement.query(By.css('.mat-body-1'));
    expect(errorTypeEl.nativeElement).toBeDefined();
    expect(errorTitleEl.nativeElement.textContent).toEqual('Error');
    expect(errorMessagEl.nativeElement.textContent).toEqual('Error occured');
  });
});

describe('NotificationComponent--without injectionToken<MAT_SNACK_BAR_DATA>', () => {
  let component: NotificationComponent;
  let fixture: ComponentFixture<NotificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NotificationComponent],
      schemas: [
        NO_ERRORS_SCHEMA
      ],
      providers: [
        { provide: MatSnackBar, useClass: MatSnackBarMock },
        { provide: MAT_SNACK_BAR_DATA, useValue: {} }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have empty title, type and message properties on component', () => {
    expect(component.title).toEqual('');
    expect(component.type).toEqual('default');
    expect(component.message).toEqual('');
  });

  it('should have title,type and message properties render on ui with empty values', () => {
    const errorTypeEl = fixture.debugElement.query(By.css('.notification__default'));
    const errorTitleEl = fixture.debugElement.query(By.css('.notification__title'));
    const errorMessagEl = fixture.debugElement.query(By.css('.mat-body-1'));
    expect(errorTypeEl.nativeElement).toBeDefined();
    expect(errorTitleEl.nativeElement.textContent).toEqual('');
    expect(errorMessagEl.nativeElement.textContent).toEqual('');
  });

});
