import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';

import { NotificationComponent } from './notification/notification.component';
import { WeatherCardComponent } from './weather-card/weather-card.component';
import { MaterialModule } from '../material/material.module';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { TempratureCardComponent } from './temprature-card/temprature-card.component';
import { SpinnerComponent } from './spinner/spinner.component';
import { ErrorComponent } from './error/error.component';

const components = [
  NotificationComponent,
  WeatherCardComponent,
  TempratureCardComponent,
  SpinnerComponent,
  ToolbarComponent,
  ErrorComponent
];

@NgModule({
  declarations: components,
  imports: [
    CommonModule,
    FlexLayoutModule,
    MaterialModule
  ],
  entryComponents: [NotificationComponent],
  exports: components
})
export class ComponentModule { }
