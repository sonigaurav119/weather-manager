import { Temperature } from 'src/app/shared/services/weather.model';

export interface TempratureCard {
    date: string;
    temp: Temperature;
    icon: string;
}
