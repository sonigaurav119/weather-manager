import { Component, ChangeDetectionStrategy, Input } from '@angular/core';
import { environment } from '../../../environments/environment';
import { TempratureCard } from './temprature-card.model';

@Component({
  selector: 'bb-temprature-card',
  templateUrl: './temprature-card.component.html',
  styleUrls: ['./temprature-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TempratureCardComponent {

  /** Path to the icons. Getting it from environment. Because path can be different for dev and prod */
  weatherIconPath = environment.weatherIconPath;

  /** Data of type TempratureCard that is required to render html */
  @Input() tempCard: TempratureCard;


}
