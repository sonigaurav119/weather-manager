import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ErrorComponent } from './error.component';
import { By } from '@angular/platform-browser';

describe('ErrorComponent', () => {
  let component: ErrorComponent;
  let fixture: ComponentFixture<ErrorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ErrorComponent ],
      schemas: [
        NO_ERRORS_SCHEMA
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ErrorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render error message on ui', () => {
    component.message = 'Error occured while fetcing data';
    fixture.detectChanges();
    const iconEl = fixture.debugElement.query(By.css('mat-icon'));
    const errorMessageEl = fixture.debugElement.query(By.css('.mat-h1'));
    expect(iconEl.nativeElement).toBeDefined();
    expect(errorMessageEl.nativeElement.textContent).toEqual(component.message);
  });

});
