import { Component, Input } from '@angular/core';

@Component({
  selector: 'bb-error',
  templateUrl: './error.component.html',
  styleUrls: ['./error.component.scss'],
})
export class ErrorComponent {

  /** Error message that gives the information to the user  */
  @Input() message: string;

}
