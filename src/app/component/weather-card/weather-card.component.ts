import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';

import { WeatherCard } from './weather-card.model';
import { WeatherIcons } from '../../shared/constants/icons-mappper';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'bb-weather-card',
  templateUrl: './weather-card.component.html',
  styleUrls: ['./weather-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush // Using onPush strategy since we want to check for change detection if new object comes
})
export class WeatherCardComponent {
  /** Data of type WeatherCard that is required to render html */
  @Input() weather: WeatherCard;
  /** Path to the icons. Getting it from environment. Because path can be different for dev and prod */
  weatherIconPath = environment.weatherIconPath;
  /** EventEmitter that emit data of type WeatherCard */
  @Output() weatherCardClick: EventEmitter<WeatherCard> = new EventEmitter();
  iconMapper = { ...WeatherIcons };

  /**
   * Notify parent component when card clicked
   * @param weather - WeatherCard
   */
  onCardClick(weather: WeatherCard) {
    this.weatherCardClick.emit(weather);
  }

}
