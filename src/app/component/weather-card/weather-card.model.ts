import { HttpErrorResponse } from '@angular/common/http';

export class WeatherCard {
    city: { id: number, name: string };
    temp: number;
    description: string;
    wind: string;
    icon: string;
    loading: boolean;
    error?: HttpErrorResponse;
}
