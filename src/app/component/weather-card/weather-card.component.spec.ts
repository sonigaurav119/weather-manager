import { NO_ERRORS_SCHEMA, ChangeDetectionStrategy } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WeatherCardComponent } from './weather-card.component';
import { By } from '@angular/platform-browser';
import { HttpErrorResponse } from '@angular/common/http';

describe('WeatherCardComponent', () => {
  let component: WeatherCardComponent;
  let fixture: ComponentFixture<WeatherCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [WeatherCardComponent],
      schemas: [
        NO_ERRORS_SCHEMA
      ]
    })
      .overrideComponent(WeatherCardComponent, { set: { changeDetection: ChangeDetectionStrategy.Default } })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WeatherCardComponent);
    component = fixture.componentInstance;
    component.weather = {
      wind: '20 meter/s',
      city: { name: 'paris', id: 123 },
      temp: 10,
      loading: false,
      icon: '01d',
      description: 'few clouds'
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('Success', () => {
    it('should have icon rendered on ui', () => {
      const icon = fixture.debugElement.query(By.css('.card__icon'));
      expect(icon.nativeElement.src).toContain(component.iconMapper[component.weather.icon]);
    });

    it('should have city name rendered on ui', () => {
      const cityName = fixture.debugElement.query(By.css('.card__content .mat-h2'));
      expect(cityName.nativeElement.textContent).toEqual(component.weather.city.name);
    });

    it('should have temp and wind rendered on ui', () => {
      const exptectedResult = `${component.weather.temp}℃ | ${component.weather.wind}`;
      const tempEl = fixture.debugElement.query(By.css('.card__content .mat-subheading-2'));
      const weatherDescriptionEl = fixture.debugElement.query(By.css('.card__content .mat-caption'));
      expect(tempEl.nativeElement.textContent.trim()).toEqual(exptectedResult);
      expect(weatherDescriptionEl.nativeElement.textContent.trim()).toEqual(component.weather.description);
    });

    it('should render loading template until data not loaded', () => {
      component.weather = { ...component.weather, loading: true };
      fixture.detectChanges();
      const loaderEl = fixture.debugElement.query(By.css('.loader'));
      expect(loaderEl.nativeElement).toBeDefined();
    });

    it('should emit weatherCardClick event when clicks on card', () => {
      component.weather = { ...component.weather, loading: true };
      const cardEl = fixture.debugElement.query(By.css('.card'));
      spyOn(component.weatherCardClick, 'emit');
      cardEl.nativeElement.click();
      fixture.detectChanges();
      expect(component.weatherCardClick.emit).toHaveBeenCalled();
      expect(component.weatherCardClick.emit).toHaveBeenCalledWith(component.weather);
    });
  });

  describe('Error', () => {
    beforeEach(() => {
      component.weather = {
        ...component.weather,
        loading: false,
        error: new HttpErrorResponse({ status: 400, statusText: 'Bad request' })
      };
      fixture.detectChanges();
    });

    it('should render error template', () => {
      const errorTemplate = fixture.debugElement.query(By.css('.card__error'));
      const errorMessage = fixture.debugElement.query(By.css('.card__error .mat-body-1'));
      expect(errorTemplate.nativeElement).toBeDefined();
      expect(errorMessage.nativeElement.textContent).toEqual(component.weather.error.statusText);
    });
  });


});
