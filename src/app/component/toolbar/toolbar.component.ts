import { Component, Input } from '@angular/core';

@Component({
  selector: 'bb-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent {

  /** The color of the toolbar. It should be exist in ThemePalette  */
  @Input() color = 'primary';

}
