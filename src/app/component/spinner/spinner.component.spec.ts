import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpinnerComponent } from './spinner.component';
import { By } from '@angular/platform-browser';

describe('SpinnerComponent', () => {
  let component: SpinnerComponent;
  let fixture: ComponentFixture<SpinnerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpinnerComponent ],
      schemas: [
        NO_ERRORS_SCHEMA
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpinnerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set color, mode, diameter, value and styleClass to mat-spinner', () => {
    component.color = 'accent';
    component.diameter = 50;
    component.mode = 'determinate';
    component.styleClass = 'bb-loader';
    component.value = 20;
    fixture.detectChanges();
    const spinnerEl = fixture.debugElement.query(By.css('mat-progress-spinner'));
    expect(spinnerEl.nativeElement.color).toEqual('accent');
    expect(spinnerEl.nativeElement.diameter).toEqual(50);
    expect(spinnerEl.nativeElement.mode).toEqual('determinate');
    expect(spinnerEl.classes['bb-loader']).toBeTruthy();
    expect(spinnerEl.nativeElement.value).toEqual(20);
  });
});
