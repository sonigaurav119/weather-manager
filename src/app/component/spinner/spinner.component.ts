import { Component, Input } from '@angular/core';

@Component({
  selector: 'bb-spinner',
  templateUrl: './spinner.component.html',
  styleUrls: ['./spinner.component.scss']
})
export class SpinnerComponent {

  /** The color of the progress spinner. It should be exist in ThemePalette  */
  @Input() color = 'primary';
  /** Mode of the progress circle */
  @Input() mode = 'indeterminate';
  /** The diameter of the progress spinner (will set width and height). */
  @Input() diameter: number;
  /** Value of the progress circle. */
  @Input() value = 0;
  /** Style class to override the css properties. */
  @Input() styleClass: string;

}
