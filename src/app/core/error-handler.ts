import { ErrorHandler, Injector, Injectable } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { LoggerService } from '../core/services/logger/logger.service';
import { NotificationService } from '../core/services/notification/notification.service';


@Injectable()
/**
 * Global error handler either server or client side error.
 * We can use to get stacktrace of error and send it to some channesl(like slack, hipchat etc)
 */
export class GlobalErrorHandler implements ErrorHandler {
    constructor(private injector: Injector) { }
    handleError(error: Error | HttpErrorResponse) {
        const logger = this.injector.get(LoggerService);
        const notification = this.injector.get(NotificationService);
        logger.log(error);
        if (error instanceof HttpErrorResponse) {
            notification.notify('error', 'Error', 'We will contact our admin about the error');
        } else {
            // Handle client side error here such as send stack trace to backend api to record errors etc.
        }
    }
}
