import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment';

/**
 * Decorator to check if application is running in prodMode and based on that log message to console
 * @param args contains target, propertyname and PropertyDescriptor
 * @returns function
 */
export function prodMode(...args: any[]) {
  const [, , propertyDesciptor] = args;
  if (environment.production) {
    propertyDesciptor.value = () => { };
  }
  return propertyDesciptor;
}

/**
 * Global service to log error.
 * Use it to send stack trace to other channels(e.g slack etc) to notify about errors.
 * Also use it to add additinal info to log linke timestamp or file etc
 */

@Injectable({
  providedIn: 'root'
})
export class LoggerService {

  constructor() { }

  @prodMode
  log(message: any) {
    console.log(message);
  }

  @prodMode
  error(error: any) {
    console.error(error);
  }

  @prodMode
  warn(message: any) {
    console.warn(message);
  }
}
