import { TestBed } from '@angular/core/testing';

import { LoggerService, prodMode } from './logger.service';
import { environment } from '../../../../environments/environment';

describe('LoggerService', () => {
  let service: LoggerService;
  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.get(LoggerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });


  it('should log message to console', () => {
    const message = 'Some test message';
    spyOn(console, 'log');
    service.log(message);
    expect(console.log).toHaveBeenCalled();
    expect(console.log).toHaveBeenCalledWith(message);
  });

  it('should log error to console', () => {
    const message = 'Some error';
    spyOn(console, 'error');
    service.error(message);
    expect(console.error).toHaveBeenCalled();
    expect(console.error).toHaveBeenCalledWith(message);
  });

  it('should log warning message to console', () => {
    const message = 'Some warning message';
    spyOn(console, 'warn');
    service.warn(message);
    expect(console.warn).toHaveBeenCalled();
    expect(console.warn).toHaveBeenCalledWith(message);
  });

});

describe('prodMode Decorator', () => {
  it('should replace properDescriptor function with empty function if prodMode is on', () => {
    const mockConsole = Object.defineProperty({}, 'log', {
      value: (message: any) => message
    });
    environment.production = true;
    const decoratedMethodInProdMode = prodMode(mockConsole, 'log', mockConsole);
    expect(decoratedMethodInProdMode.value()).toEqual(undefined);
    environment.production = false;

  });

  it('should not replace properDescriptor function with empty function if prodMode is on', () => {
    const mockConsole = Object.defineProperty({}, 'log', {
      value: (message: any) => message
    });
    environment.production = false;
    const decoratedMethodInDevMode = prodMode(mockConsole, 'log', mockConsole.log);
    expect(decoratedMethodInDevMode('test')).toEqual('test');
  });
});
