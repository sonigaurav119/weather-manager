import { Injectable, NgZone } from '@angular/core';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';
import { NotificationComponent } from 'src/app/component/notification/notification.component';

/** Default configuration for snackbar */
const DEFAULT_CONFIG: MatSnackBarConfig = {
  duration: 3000,
  horizontalPosition: 'center',
  verticalPosition: 'bottom'
};

/**
 * Notification service used to show toast message to user
 */
@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  constructor(private snackBar: MatSnackBar, private zone: NgZone) { }

  /**
   * Show notification to the user. It can be error, success or warning notification.
   * @param type reprents type of notification. For example => success, error, warning
   * @param message message that needs to be displayed on the ui.
   */
  notify(type: string, title: string, message: string, config?: MatSnackBarConfig) {
    const data = { message, title, type };
    config = { ...config, panelClass: `notification-${type.toLowerCase()}`, data };
    const snackbarConfig = { ...DEFAULT_CONFIG, ...config };
    // Angular hack to run somecode asynchronously
    this.zone.run(() => {
      this.snackBar.openFromComponent(NotificationComponent, snackbarConfig);
    });
  }
}
