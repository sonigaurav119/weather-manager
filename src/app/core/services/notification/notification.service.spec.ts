import { TestBed } from '@angular/core/testing';

import { NotificationService } from './notification.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSnackBarMock } from '../../../../helpers/mocks/services/mat-snackbar.service.mock';

describe('NotificationService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      {provide: MatSnackBar, useClass: MatSnackBarMock}
    ]
  }));

  it('should be created', () => {
    const service: NotificationService = TestBed.get(NotificationService);
    expect(service).toBeTruthy();
  });

  it('should call material snackbar service to show message to user', () => {
    const service: NotificationService = TestBed.get(NotificationService);
    const snackbar: MatSnackBar = TestBed.get(MatSnackBar);
    spyOn(snackbar, 'openFromComponent');
    service.notify('error', 'Error', 'Something went wrong!');
    expect(snackbar.openFromComponent).toHaveBeenCalled();
  });
});
