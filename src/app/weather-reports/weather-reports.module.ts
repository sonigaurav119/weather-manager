import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { SharedModule } from '../shared/shared.module';
import { WeatherReportsComponent } from './weather-reports.component';
import { WeatherReportService } from './weather-report.service';

@NgModule({
  declarations: [WeatherReportsComponent],
  imports: [
    RouterModule,
    CommonModule,
    SharedModule
  ],
  providers: [WeatherReportService]
})
export class WeatherReportsModule { }
