import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { map, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';

import { CITIES } from '../shared/constants/cities';
import { WeatherService } from '../shared/services/weather.service';
import { WeatherCard } from '../component/weather-card/weather-card.model';
import { WeatherReportService } from './weather-report.service';
import { LoggerService } from '../core/services/logger/logger.service';

@Component({
  selector: 'bb-weather-reports',
  templateUrl: './weather-reports.component.html',
  styleUrls: ['./weather-reports.component.scss']
})
export class WeatherReportsComponent implements OnInit {

  // Initially set card to loading state to show loader
  weatherCards: WeatherCard[] = Array(CITIES.length).fill({ loading: true });

  constructor(
    private weatherService: WeatherService,
    private weatherReportService: WeatherReportService,
    private logger: LoggerService,
    private router: Router) { }

  ngOnInit() {
    // Need to call api for each city, since open weather api doesn't have option to group
    // cities in free plan.
    CITIES.forEach(city => {
      this.getWeatherCard(city);
    });
  }

  getWeatherCard(city: { id: number, name: string }) {
    const params = this.weatherReportService.getParams({ id: city.id, unit: 'metric' });
    this.weatherService.getCurrentWeather(params).pipe(
      map(res => this.weatherReportService.transformToWeatherCard(res)),
      catchError(error => {
        // hide loader in case of error.
        // Also we can map error to custom error.
        this.weatherCards = Array(CITIES.length).fill({ loading: false, error });
        this.logger.log('Error occured while fetching data');
        return throwError(error);
      })
    ).subscribe(weather => {
      const weatherCardIndex = this.weatherCards.findIndex(weatherCard => weatherCard.loading);
      this.weatherCards[weatherCardIndex] = { ...weather };
      this.logger.log('Weather data fetched successfully');
      // We can store weatherReports in service.
      // If there is requirement to use same data in different place, then we don't need to call api again.
      // Here we don't have such kind of requirement. So we are not storing it.
    });
  }

  /**
   * Navigate user to weathreForcast route
   * @param weather of type WeatherCard
   */
  navigateToForcastDetails(weather: WeatherCard) {
    this.router.navigate(['forcast', weather.city.id]);
  }

}
