import { NO_ERRORS_SCHEMA } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { WeatherService } from '../shared/services/weather.service';
import { MockWeatherService } from '../../helpers/mocks/services/weather.service.mock';

import { WeatherReportsComponent } from './weather-reports.component';
import { throwError } from 'rxjs';
import { WeatherReportService } from './weather-report.service';
import { HttpErrorResponse } from '@angular/common/http';

const moduleImports = [HttpClientTestingModule, RouterTestingModule];
const components = [WeatherReportsComponent];
const services = [{ provide: WeatherService, useClass: MockWeatherService },
  WeatherReportService];

describe('Unit - WeatherReportsComponent', () => {
  const cities = ['paris', 'amsterdam', 'Salzburg', 'Nuremberg', 'barcelona'];
  let component: WeatherReportsComponent;
  let fixture: ComponentFixture<WeatherReportsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: moduleImports,
      declarations: components,
      schemas: [
        NO_ERRORS_SCHEMA
      ],
      providers: services
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WeatherReportsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have list of weatherCards', () => {
    expect(component.weatherCards.length).toEqual(5);
  });

  it('should have list of cities inside weather reports', () => {
    component.weatherCards.forEach((card, index: number) => {
      expect(cities.indexOf(card.city.name)).toBeGreaterThan(-1);
    });
  });

  it('should have transformed weather report', () => {
    const firstWeatherCard = component.weatherCards[0];
    expect(firstWeatherCard.city).toBeDefined();
    expect(firstWeatherCard.wind).toBeDefined();
    expect(firstWeatherCard.temp).toBeDefined();
    expect(firstWeatherCard.icon).toBeDefined();
    expect(firstWeatherCard.loading).toBeDefined();
  });

  it('should have weather cards rendered on ui', () => {
    const weatherCard = fixture.debugElement.query(By.css('bb-weather-card'));
    expect(weatherCard.nativeElement).toBeDefined();
  });

});


describe('Unit - WeatherReportsComponent--Error', () => {
  const error = new HttpErrorResponse({status: 400, statusText: 'Bad request'});
  let weatherService: WeatherService;
  let component: WeatherReportsComponent;
  let fixture: ComponentFixture<WeatherReportsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: moduleImports,
      declarations: components,
      schemas: [
        NO_ERRORS_SCHEMA
      ],
      providers: services
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WeatherReportsComponent);
    component = fixture.componentInstance;
    weatherService = TestBed.get(WeatherService);
    fixture.detectChanges();
  });

  it('should have set card loading state to false in case of error', () => {
    spyOn(weatherService, 'getCurrentWeather').and.returnValue(throwError(error));
    component.getWeatherCard({ id: 1233, name: 'Amsterdam' });
    component.weatherCards.forEach(card => {
      expect(card.loading).toEqual(false);
      expect(card.error).toEqual(error);
    });
    expect(weatherService.getCurrentWeather).toHaveBeenCalled();
  });

});
