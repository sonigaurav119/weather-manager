import { WeatherReport } from '../shared/services/weather.model';
import { WeatherCard } from '../component/weather-card/weather-card.model';

export class WeatherReportService {

  /**
   * Transform the response coming from API to a format that is understand by weatherCard
   * @param report - Array of WeatherReport
   * @returns WeatherCard
   */
  transformToWeatherCard(report: WeatherReport): WeatherCard {
    return {
      icon: report.weather[0].icon,
      city: { id: report.id, name: report.name },
      temp: report.main.temp,
      wind: `${report.wind.speed} meter/s`,
      description: report.weather[0].description,
      loading: false
    };
  }

  /**
   * Convert an Object to key value pair.e.g => [{key: id, value: 1}, {key: unit: value: metric}]
   * @param rawObject of type Object. e.g => {id: 1, unit: 'metric}
   * @return Array of params
   */
  getParams(rawObject: any): { key: string, value: any }[] {
    return Object.keys(rawObject).map(key => ({ key, value: rawObject[key] }));
  }
}
