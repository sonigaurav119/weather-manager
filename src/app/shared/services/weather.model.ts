export interface Coord {
    lat: number;
    lon: number;
}

export interface City {
    coords: Coord;
    country: string;
    id: number;
    name: string;
}

export interface Temperature {
    humidity: number;
    pressure: number;
    temp: number;
    temp_max: number;
    temp_min: number;
    grnd_level: number;
}

export interface Wind {
    deg: number;
    speed: number;
}

export interface Weather {
    id: number;
    icon: string;
    main: string;
    description: string;
}

export interface WeatherReport {
    coord: Coord;
    main: Temperature;
    wind: Wind;
    weather: Weather[];
    id: number;
    clouds: { all: number };
    name: string;
    visibility: number;
}

export interface WeatherForcast {
    dt: number;
    clouds: { all: number };
    dt_txt: string;
    main: Temperature;
    weather: Weather[];
    wind: Wind;
}

export class WeatherReportModel implements WeatherReport {
    coord: Coord;
    main: Temperature;
    wind: Wind;
    weather: Weather[];
    id: number;
    clouds: { all: number };
    name: string;
    visibility: number;

    constructor({ coord = { lat: null, lon: null },
        main = {
            humidity: null, grnd_level: null, pressure: null, temp: null, temp_max: null, temp_min: null
        },
        wind = { deg: null, speed: null }, id = null, clouds = { all: null },
        weather = [{ id: null, main: '', description: '', icon: '' }],
        visibility = null, name = '' } = {}) {
        this.coord = coord;
        this.main = main;
        this.weather = weather;
        this.id = id;
        this.clouds = clouds;
        this.name = name;
        this.visibility = visibility;
        this.wind = wind;
    }
}

export class WeatherForcastModel {
    count: number;
    city: City;
    forcastData: WeatherForcast[];
    constructor({
        count = null, city = null, forcastData = []
    } = {}) {
        this.count = count;
        this.city = city;
        this.forcastData = forcastData;
    }
}

