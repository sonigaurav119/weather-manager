import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { WeatherService } from './weather.service';
import { weatherReport, city } from '../../../builders/weather-report-builder';
import { weatherForcast } from 'src/builders/weather-forcast-builder';
import { throwError } from 'rxjs';
import { WeatherForcastModel, WeatherReportModel } from './weather.model';
import { HttpErrorResponse } from '@angular/common/http';

describe('WeatherService', () => {
  let service: WeatherService;
  let httpMock: HttpTestingController;
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
    providers: [WeatherService]
  }));

  beforeEach(() => {
    service = TestBed.get(WeatherService);
    httpMock = TestBed.get(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return observable of weatherReports', () => {
    const weatherReports = { ...weatherReport().getReport() };
    const params = [
      { key: 'id', value: 12345 },
      { key: 'units', value: 'metric' }
    ];
    service.getCurrentWeather(params).subscribe(res => {
      expect(res.name).toEqual('paris');
      expect(res.id).toEqual(524901);
    });
    const queryString = `id=${12345}&units=metric&appid=${service.appId}`;
    const url = `${service.apiUrl}/weather?${queryString}`;
    const req = httpMock.expectOne(url);
    // Expect request to have been called with given url
    expect(req.request.method).toEqual('GET');
    req.flush(weatherReports);
    httpMock.verify();
  });

  it('should call handle error in case of error', () => {
    const data = 'invalid params';
    const mockErrorResponse = { status: 400, statusText: 'Bad Request' };
    spyOn(service, 'handleError').and.returnValue(throwError('invalid params'));
    let response;
    const params = [
      { key: 'id', value: 12345 },
      { key: 'units', value: 'metric' }
    ];
    service.getCurrentWeather(params).subscribe(res => {
      response = res;
    }, err => {
      expect(err).toEqual(data);
    });
    const queryString = `id=${12345}&units=metric&appid=${service.appId}`;
    const url = `${service.apiUrl}/weather?${queryString}`;
    const req = httpMock.expectOne(url);
    req.flush(data, mockErrorResponse);
    expect(service.handleError).toHaveBeenCalled();
  });

  it('should return observable of weatherForecast', () => {
    const res = { city, cnt: 1, list: [{ ...weatherForcast().getForcast() }] };
    service.getweatherForcast('2759794').subscribe(res => {
      const forcast = res.forcastData;
      expect(res.city.name).toEqual('Amstedam');
      expect(res.city.id).toEqual(2759794);
      expect(forcast.length).toEqual(1);
      expect(forcast[0].dt).toEqual(1558645200);
      expect(forcast[0].dt_txt).toEqual('2019-05-23 21:00:00');
      expect(forcast[0].main.temp).toEqual(-10.5);
    });
    const queryString = `id=${2759794}&appid=${service.appId}`;
    const url = `${service.apiUrl}/forecast/hourly?${queryString}`;
    // Expect request has been called with given url
    const req = httpMock.expectOne(url);
    expect(req.request.method).toEqual('GET');
    req.flush(res);
    httpMock.verify();
  });


  it('should call handle error in case of error while fetching forcasted result', () => {
    const data = 'invalid params';
    const mockErrorResponse = { status: 401, statusText: 'UnAuthorized' };
    let response;
    spyOn(service, 'handleError').and.returnValue(throwError('invalid params'));
    service.getweatherForcast('2759794').subscribe(res => {
      response = res;
    }, err => {
      expect(err).toEqual(data);
    });
    const queryString = `id=${2759794}&appid=${service.appId}`;
    const url = `${service.apiUrl}/forecast/hourly?${queryString}`;
    const req = httpMock.expectOne(url);
    req.flush(data, mockErrorResponse);
    expect(service.handleError).toHaveBeenCalled();
  });

  it('should throw error', () => {
    const mockErrorResponse = new HttpErrorResponse({ error: 'Bad request' });
    const err = service.handleError(mockErrorResponse);
    err.subscribe(res => {
      expect(res).toEqual('Bad request');
    });
  });

  it('should set WeatherReportModel values to null if nothing passed to constructor', () => {
    const weatherReportModel = new WeatherReportModel();
    expect(weatherReportModel.coord.lat).toBeNull();
    expect(weatherReportModel.coord.lon).toBeNull();
    Object.keys(weatherReportModel.main).forEach(key => {
      expect(weatherReportModel.main[key]).toBeNull();
    });
    Object.keys(weatherReportModel.wind).forEach(key => {
      expect(weatherReportModel.wind[key]).toBeNull();
    });
    Object.keys(weatherReportModel.weather[0]).forEach(key => {
      if (key !== 'id') {
        expect(weatherReportModel.weather[0][key]).toEqual('');
      }
    });
    expect(weatherReportModel.weather[0].id).toBeNull();
    expect(weatherReportModel.id).toBeNull();
    expect(weatherReportModel.clouds.all).toBeNull();
    expect(weatherReportModel.visibility).toBeNull();
    expect(weatherReportModel.name).toEqual('');
  });

  it('should set weatherForcaseModel values to null if nothing passed to constructor', () => {
    const forecastModel = new WeatherForcastModel();
    expect(forecastModel.city).toBeNull();
    expect(forecastModel.count).toBeNull();
    expect(forecastModel.forcastData).toEqual([]);
  });

});
