import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { WeatherReport, City, WeatherForcast, WeatherReportModel, WeatherForcastModel } from './weather.model';
import { catchError, map } from 'rxjs/operators';
import { throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
/**
 *  A generic service to fetch info related to weather via (https://openweathermap.org/api)
 *  and map it to the format that is understand by app.
 *  The purpose of mapping is if tommorrow  a requirement comes to change open weather api
 *  to some other api then we need to change in one place.
 */
export class WeatherService {

  readonly apiUrl = environment.endPoint;
  readonly appId = environment.appId;
  constructor(private http: HttpClient) { }

  /**
   * Generic method to get current weather based on the city name, city id, lat|long etc.
   * @param params in format of key and value. e.g => {key: city, value: 'Amsterdam, NL'}
   */
  getCurrentWeather(params: { key: string, value: any }[]) {
    let queryParams = new HttpParams();
    params.forEach(param => queryParams = queryParams.append(param.key, param.value));
    queryParams = queryParams.append('appid', this.appId);
    const url = `${this.apiUrl}/weather`;
    return this.http.get<WeatherReport>(url, { params: queryParams }).pipe(
      // The purpose of mapping is if tommorrow  a requirement comes to change open weather api
      // to some other api then we need to change in one place.
      map(res => new WeatherReportModel(res)),
      catchError(this.handleError));
  }

  /**
   * Generic method to get forecast of next 4 days of weather
   * @param cityId id of city as per open weather api
   */
  getweatherForcast(cityId: string) {
    const url = `${this.apiUrl}/forecast/hourly`;
    const queryParams = new HttpParams().append('id', cityId).append('appid', this.appId);
    return this.http.get<{ cnt: number, city: City, list: WeatherForcast[] }>(url, { params: queryParams }).pipe(
      // The purpose of mapping is if tommorrow  a requirement comes to change open weather api
      // to some other api then we need to change in one place.
      map(res => new WeatherForcastModel({ count: res.cnt, city: res.city, forcastData: res.list })),
      catchError(this.handleError)
    );
  }

  /**
   * Handle errors that are thrown by angular httpClient.
   * @param error an httpErrorResponse object
   */
  handleError(error: HttpErrorResponse) {
    return throwError(error);
  }
}
