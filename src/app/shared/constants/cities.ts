// Store cities in constant so that we can refer them from single place.
// And also it makes things easy to scale
export const CITIES: { name: string, id: number }[] = [
    { name: 'paris', id: 6455259 },
    { name: 'amsterdam', id: 2759794 },
    { name: 'berlin', id: 2950159 },
    { name: 'Salzburg', id: 2766823 },
    { name: 'barcelona', id: 6356055 }
];
