import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';

import { ComponentModule } from '../component/component.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    FlexLayoutModule,
    ComponentModule
  ],
  exports: [
    ComponentModule,
    FlexLayoutModule
  ]
})
export class SharedModule { }
